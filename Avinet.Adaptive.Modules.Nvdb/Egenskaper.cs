﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Data;
using System.IO;
using db = Cartomatic.Utils.Db;
using Cartomatic.Utils;
using Cartomatic.Utils.OpsIO;

namespace Avinet.Adaptive.Modules.Nvdb
{
    class Egenskaper: Nvdb
    {
        private string CreateUrl(int vegobjekttype)
        {
            return String.Format("https://www.vegvesen.no/nvdb/api/v2/vegobjekttyper/{0}", vegobjekttype);
        }

        internal OperationOutput GetEgenskapTable(Db.DbCredentials dbc, int vegobjekttypeid)
        {
            OperationOutput output = new OperationOutput();

            var webrequest = (HttpWebRequest) System.Net.WebRequest.Create(CreateUrl(vegobjekttypeid));
            using (var response = webrequest.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var result = reader.ReadToEnd();

                    var rootvegobjekttyper = JsonConvert.DeserializeObject<RootVegobjekttyper>(result);

                    output.SetData("stedfesting", rootvegobjekttyper.stedfesting.ToUpper());
                    output.SetData("vegobjekttype", rootvegobjekttyper.sosinavn.ToUpper());
                    output.SetData("egenskaper", getEgenskaper(rootvegobjekttyper.egenskapstyper));

                    output.success = true;
                }
            }

            return output;
        }


        internal OperationOutput CheckEgenskapTable(Db.DbCredentials dbc, int vegobjekttypeid)
        {
            OperationOutput output = new OperationOutput();

            var webrequest = (HttpWebRequest) System.Net.WebRequest.Create(CreateUrl(vegobjekttypeid));
            using (var response = webrequest.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var result = reader.ReadToEnd();

                    var rootvegobjekttyper = JsonConvert.DeserializeObject<RootVegobjekttyper>(result);
                    var type = new Enums.Type();

                    switch (rootvegobjekttyper.stedfesting.ToUpper())
                    {
                        case "LINJE":
                            type = Enums.Type.line;
                            break;
                        case "PUNKT":
                            type = Enums.Type.point;
                            break;
                        default:
                            output.success = false;
                            output.exception.msg = "Unknown geometry type!";
                            break;
                    }

                    var vegobjekttype = rootvegobjekttyper.sosinavn.ToLower();

                    var g = new Generic();
                    output = g.ExistTable(dbc, vegobjekttype);
                    if (!output.success)
                    {
                        var e = getEgenskaper(rootvegobjekttyper.egenskapstyper);
                        output = CreateEgenskaper(dbc, vegobjekttype, e, type );
                        if (!output.success)
                        {
                            return output;
                        }
                    }

                    output.SetData("vegobjekttype", vegobjekttype);
                }
            }

            return output;
        }

        internal List<EgenskapObject> getEgenskaper(List<object> d)
        {
            var result = new List<EgenskapObject>();

            for (int i = 0; i < d.Count; i++)
            {
                Dictionary<string, object> collection = JsonConvert.DeserializeObject<Dictionary<string, object>>(d[i].ToString());

                var r = new EgenskapObject();

                foreach (var item in collection)
                {
                    if (item.Key == "id")
                    {
                        r.id = Convert.ToInt32(item.Value);
                    }
                    else if (item.Key == "navn")
                    {
                        r.navn = item.Value.ToString();
                    }
                    else if (item.Key == "datatype")
                    {
                        r.datatype = Convert.ToInt32(item.Value);
                    }
                    else if (item.Key == "verdi")
                    {
                        r.verdi = item.Value.ToString();
                    }
                    else if (item.Key == "desimaler")
                    {
                        r.desimaler = Convert.ToInt16(item.Value);
                    }
                    else if (item.Key == "sosinavn")
                    {
                        r.sosinavn = item.Value.ToString();
                    }
                    else if (item.Key == "feltlengde")
                    {
                        r.feltlengde = Convert.ToInt16(item.Value);
                    }
                }

                result.Add(r);
            }

            return result;
        }

        private string SqlCreateEgenskapTable(string tablename, List<EgenskapObject> egenskaper, Enums.Type type)
        {
            string sql = String.Format("CREATE TABLE {0}.{1} (", _SCHEMA, tablename);

            sql += "id serial NOT NULL, kortform text, objektid integer, kommune integer,";

            foreach (var item in egenskaper)
            {
                var s = "";
                var fieldname = Utils.fixFieldName(item.navn);
                
                if (item.datatype == 2)
                    s = String.Format("{0} numeric({1},{2}),", fieldname, item.feltlengde, item.desimaler);
                else if (item.datatype == 2)
                    s = fieldname + " integer,";
                else if (item.datatype == 8)    // date
                    s = fieldname + " date,";
                else
                    s = fieldname + " text,";

                sql += s;
            }

            if (type == Enums.Type.line)
            {
                sql += "geom geometry(LineString, 32633),";
            }
            else
            {
                sql += "geom geometry(POINT, 32633),";
            }

            sql += String.Format("CONSTRAINT {0}_pkey PRIMARY KEY (id)", tablename);
            sql += " );";

            return sql;
        }

        internal string SqlInsertEgenskap( string tablename, VegsegmentObject vs, int objektid, int kommune, List<EgenskapObject> egenskaper)
        {
            string sql = String.Format("INSERT INTO {0}.{1} (", _SCHEMA, tablename);

            sql += "kortform, objektid, kommune,";

            foreach (var item in egenskaper)
            {
                var s = @" ""FIELD"" ,";
                s = s.Replace("FIELD", Utils.fixFieldName(item.navn));
                sql += s;
            }
            //sql = sql.Substring(0, sql.Length - 1);
            sql += " geom) VALUES ('" + vs.kortform + "'," + objektid + "," + kommune + ",";

            foreach (var item in egenskaper)
            {
                var s = "";
                
                if (item.datatype == 2 && item.desimaler == 0) {
                    if (item.verdi.Contains(","))
                        item.verdi = item.verdi.Replace(",",".");
                    s = String.Format("{0},", item.verdi);
                }
                else if (item.datatype == 2 && item.desimaler != 0)
                    s = String.Format("{0},",item.verdi);
                else if (item.datatype == 8)    // date
                    s = String.Format("'{0}',", item.verdi);
                else
                    s = String.Format("'{0}',", Utils.fixFieldValue(item.verdi));
                //s = s.Replace("VAL", Utils.fixFieldValue(item.verdi));

                sql += s;
            }

            sql += "ST_GeomFromText(ST_AsEWKT(ST_Force2D(ST_GeomFromEWKT('" + vs.geometri + "'))),32633));";
            //sql += "ST_GeomFromText('" + vs.geometri + "') );";

            return sql;
        }

        private OperationOutput CreateEgenskaper(db.DbCredentials dbc, string tablename, List<EgenskapObject> egenskaper, Enums.Type type)
        {
            var output = new OperationOutput();

            dynamic conn = db.GetDbConnectionObject(dbc);
            try
            {
                conn.Open();

                dynamic cmd = db.GetDbCommandObject(dbc);
                cmd.Connection = conn;

                dynamic da = db.GetDbDataAdapterObject(dbc);
                da.SelectCommand = cmd;

                string sql = SqlCreateEgenskapTable(tablename, egenskaper,type);

                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();

                output.success = true;
            }
            catch (Exception ex)
            {
                output.exception = new Cartomatic.Utils.Exceptions.Exception(ex.Message);
            }
            finally
            {
                db.CloseConnection(conn);
            }

            return output;
        }

        private OperationOutput CreateTable(Db.DbCredentials dbc, List<object> egenskapstyper)
        {
            OperationOutput output = new OperationOutput();

            foreach (var item in egenskapstyper)
            {
                var result = new List<EgenskapObject>();

                Dictionary<string, object> collection = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());

            }
            return null;
        }

        public OperationOutput RemoveData(Db.DbCredentials dbc, string egenskapstabell, int kommune)
        {
            var output = new OperationOutput();

            string sql2 = String.Format("DELETE FROM {0}.{1} WHERE kommune={2}", _SCHEMA, egenskapstabell, kommune);
            output = Generic.DoSql(dbc, sql2);
            if (!output.success)
            {
                return output;
            }
            return output;
        }

        public OperationOutput RemoveIndex(Db.DbCredentials dbc, string egenskapstabell)
        {
            var output = new OperationOutput();

            string sql1 = String.Format("DROP INDEX IF EXISTS {0}.{1}_idx", _SCHEMA, egenskapstabell);
            output = Generic.DoSql(dbc, sql1);
            if (!output.success)
            {
                return output;
            }

            string sql2 = String.Format("DROP INDEX IF EXISTS {0}.{1}_gist", _SCHEMA, egenskapstabell);
            output = Generic.DoSql(dbc, sql2);
            if (!output.success)
            {
                return output;
            }

            return output;
        }

        public OperationOutput CreateIndex(Db.DbCredentials dbc, string egenskapstabell)
        {
            var output = new OperationOutput();

            string sql1 = String.Format("CREATE INDEX {1}_idx ON {0}.{1} USING btree (id);", _SCHEMA, egenskapstabell);
            output = Generic.DoSql(dbc, sql1);
            if (!output.success)
            {
                return output;
            }

            string sql2 = String.Format("CREATE INDEX {1}_gist ON {0}.{1} USING gist (geom);",_SCHEMA, egenskapstabell);
            output = Generic.DoSql(dbc, sql2);
            if (!output.success)
            {
                return output;
            }
            
            return output;
        }
    }
}
