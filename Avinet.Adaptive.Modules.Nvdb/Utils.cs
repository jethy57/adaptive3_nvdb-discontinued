﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avinet.Adaptive.Modules.Nvdb
{
    static class Utils
    {
        static internal string fixFieldName(string fieldname)
        {
            fieldname = fieldname.ToLower();
            fieldname = fieldname.Trim();

            fieldname = fieldname.Replace("æ", "ae");
            fieldname = fieldname.Replace("ø", "oe");
            fieldname = fieldname.Replace("å", "aa");
            fieldname = fieldname.Replace("(", "_");
            fieldname = fieldname.Replace(")", "_");
            fieldname = fieldname.Replace("/", "_");
            fieldname = fieldname.Replace(" ", "_");
            fieldname = fieldname.Replace(",", "_");
            fieldname = fieldname.Replace("-", "_");
            fieldname = fieldname.Replace(".", "_");

            fieldname = fieldname.Replace("____", "_");
            fieldname = fieldname.Replace("___", "_");
            fieldname = fieldname.Replace("__", "_");

            return fieldname;
        }

        static internal string fixFieldValue(string value)
        {
            value = value.Replace("'", "''");

            return value;
        }
    }
}
