﻿using Cartomatic.Utils.OpsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db = Cartomatic.Utils.Db;

namespace Avinet.Adaptive.Modules.Nvdb
{
    internal class Generic : Nvdb
    {
        internal OperationOutput ExistTable(db.DbCredentials dbc, string tablename)
        {
            var output = new OperationOutput();

            dynamic conn = db.GetDbConnectionObject(dbc);
            try
            {
                conn.Open();

                dynamic cmd = db.GetDbCommandObject(dbc);
                cmd.Connection = conn;

                dynamic da = db.GetDbDataAdapterObject(dbc);
                da.SelectCommand = cmd;

                cmd.CommandText = String.Format("select case when exists((select * from information_schema.tables where upper(table_schema) = upper('{0}') and upper(table_name) = upper('{1}'))) then 1 else 0 end", _SCHEMA, tablename);

                output.success = (int) cmd.ExecuteScalar() == 1;
            }
            catch (Exception ex)
            {
                output.exception = new Cartomatic.Utils.Exceptions.Exception(ex.Message);
            }
            finally
            {
                db.CloseConnection(conn);
            }

            return output;
        }

        internal static OperationOutput DoSql(db.DbCredentials dbc, string sql)
        {
            var output = new OperationOutput();

            dynamic conn = db.GetDbConnectionObject(dbc);
            try
            {
                conn.Open();

                dynamic cmd = db.GetDbCommandObject(dbc);
                cmd.Connection = conn;

                dynamic da = db.GetDbDataAdapterObject(dbc);
                da.SelectCommand = cmd;

                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();

                output.success = true;
            }
            catch (Exception ex)
            {
                output.exception = new Cartomatic.Utils.Exceptions.Exception(ex.Message);
            }
            finally
            {
                db.CloseConnection(conn);
            }

            return output;
        }

        //internal OperationOutput Prepare(db.DbCredentials dbc, string vegobjekttype, int vegobjekttypeid, int kommune)
        //{
        //    var output = new OperationOutput();

        //    string sql0 = String.Format("DELETE FROM {0}.{1} WHERE vegobjekttypeid = '{2}' AND kommune = {3}", _SCHEMA, _TABLE_NAME, vegobjekttypeid, kommune);
        //    output = DoSql(dbc, sql0);
        //    if (!output.success)
        //    {
        //        return output;
        //    }

        //    var tablename = Utils.fixTableName(vegobjekttype);

        //    string sql1 = String.Format("DELETE FROM {0}.{1} WHERE kommune = {2}", _SCHEMA, tablename, kommune);
        //    output = DoSql(dbc, sql1);
        //    if (!output.success)
        //    {
        //        return output;
        //    }

        //    return output;
        //}

    }
}
