﻿using Cartomatic.Utils.OpsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db = Cartomatic.Utils.Db;

namespace Avinet.Adaptive.Modules.Nvdb
{
    internal class Vegreferanse: Nvdb
    {
        internal OperationOutput Exist(db.DbCredentials dbc)
        {
            var g = new Generic();
            return g.ExistTable(dbc, _TABLE_NAME);
        }

        private string SqlCreateVegreferanse()
        {
            string sql = String.Format(@"CREATE TABLE {0}.{1}
                (
                    id serial NOT NULL,
                    objektid integer,
                    objekttypeid integer,
                    fylke integer,
                    kommune integer,
                    kategori text,
                    status text,
                    nummer integer,
                    hp integer,
                    fra_meter integer,
                    til_meter integer,
                    meter integer,
                ", _SCHEMA, _TABLE_NAME);

            sql += "CONSTRAINT " + _TABLE_NAME + "_pkey PRIMARY KEY (id)";
            sql += " );";

            return sql;
        }

        internal string SqlInsertVegsegment(VegsegmentObject vegsegment, int objectid, int objekttypeid)
        {
            string sql = @"INSERT INTO " + _SCHEMA + "." + _TABLE_NAME + @"(
                objektid,objekttypeid,fylke,kommune,kategori,status,hp,fra_meter,til_meter,
                meter)
                VALUES (" +
                vegsegment.objektid + "," +
                vegsegment.objekttypeid + " ," +
                vegsegment.fylke + "," +
                vegsegment.kommune + "," +
                "'" + vegsegment.kategori + "' ," +
                "'" + vegsegment.status + "' ," +
                vegsegment.hp + "," +
                vegsegment.fra_meter + "," +
                vegsegment.til_meter + "," +
                vegsegment.meter + ")";

            return sql;
        }

        internal OperationOutput CreateVegreferanse(db.DbCredentials dbc)
        {
            var output = new OperationOutput();

            dynamic conn = db.GetDbConnectionObject(dbc);
            try
            {
                conn.Open();

                dynamic cmd = db.GetDbCommandObject(dbc);
                cmd.Connection = conn;

                dynamic da = db.GetDbDataAdapterObject(dbc);
                da.SelectCommand = cmd;

                string sql = SqlCreateVegreferanse();

                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();

                output.success = true;
            }
            catch (Exception ex)
            {
                output.exception = new Cartomatic.Utils.Exceptions.Exception(ex.Message);
            }
            finally
            {
                db.CloseConnection(conn);
            }

            return output;
        }

        public OperationOutput CreateIndex(db.DbCredentials dbc)
        {
            var output = new OperationOutput();

            string sql1 = String.Format("CREATE INDEX vegreferanse_idx ON {0}.vegreferanse USING btree (id);", _SCHEMA);
            output = Generic.DoSql(dbc, sql1);
            if (!output.success)
            {
                return output;
            }
            return output;
        }

        public OperationOutput RemoveIndex(db.DbCredentials dbc)
        {
            var output = new OperationOutput();

            string sql1 = String.Format("DROP INDEX IF EXISTS {0}.vegreferanse_idx;", _SCHEMA);
            output = Generic.DoSql(dbc, sql1);
            if (!output.success)
            {
                return output;
            }
            return output;
        }

        public OperationOutput RemoveData(db.DbCredentials dbc, int objekttypeid, int kommune)
        {
            var output = new OperationOutput();

            string sql1 = String.Format("DELETE FROM {0}.vegreferanse WHERE kommune={1} AND objekttypeid={2}", _SCHEMA, kommune, objekttypeid);
            output = Generic.DoSql(dbc, sql1);
            if (!output.success)
            {
                return output;
            }
            return output;
        }

    }
}
