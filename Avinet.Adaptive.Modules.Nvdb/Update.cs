﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Data;
using System.IO;
using db = Cartomatic.Utils.Db;
using Cartomatic.Utils;
using Cartomatic.Utils.OpsIO;

namespace Avinet.Adaptive.Modules.Nvdb
{
    public class Update: Nvdb
    {
        public void UpdateAll()
        {
            var nvdb_kommuner = System.Configuration.ConfigurationManager.ConnectionStrings["nvdb_kommuner"].ToString();
            var nvdb_vegobjekttyper = System.Configuration.ConfigurationManager.ConnectionStrings["nvdb_vegobjekttyper"].ToString();

            var kommuner = nvdb_kommuner.Split(',').Select(s => s.Substring(0, 2)).ToList();
            var vegobjekttyper = nvdb_vegobjekttyper.Split(',').Select(s => s.Substring(0, 2)).ToList();

            foreach (var vegobjekttype in vegobjekttyper)
            {
                foreach (var kommune in kommuner)
                {
                    //Update(vegobjekttype, kommune);
                }
            }
        }

        public OperationOutput PrepareVegreferanse(Db.DbCredentials dbc)
        {
            OperationOutput output = new OperationOutput();

            var v = new Vegreferanse();
            output = v.Exist(dbc);
            if (!output.success)
            {
                output = v.CreateVegreferanse(dbc);
                if (!output.success)
                {
                    return output;
                }
            }

            output = v.RemoveIndex(dbc);
            if (!output.success)
            {
                return output;
            }

            return output;
        }

        public OperationOutput PrepareKommuneAndEgenskap(Db.DbCredentials dbc, int vegobjekttypeid, string egenskapstabell, int kommune)
        {
            OperationOutput output = new OperationOutput();

            var v = new Vegreferanse();
            output = v.RemoveData(dbc, vegobjekttypeid, kommune);
            if (!output.success)
            {
                return output;
            }

            var e = new Egenskaper();
            output = e.RemoveData(dbc, egenskapstabell, kommune);
            if (!output.success)
            {
                return output;
            }

            return output;
        }

        public OperationOutput RemoveIndexForEgenskaper(Db.DbCredentials dbc, string egenskapstabell)
        {
            var e = new Egenskaper();
            return e.RemoveIndex(dbc, egenskapstabell);
        }

        public OperationOutput CreateIndexForEgenskaper(Db.DbCredentials dbc, string egenskapstabell)
        {
            var e = new Egenskaper();
            return e.CreateIndex(dbc, egenskapstabell);
        }

        public OperationOutput CreateIndexForVegreferanse(Db.DbCredentials dbc)
        {
            var v = new Vegreferanse();
            return v.CreateIndex(dbc);
        }

        public OperationOutput RemoveDataForVegreferanse(db.DbCredentials dbc, int vegobjekttypeid, int kommune)
        {
            var v = new Vegreferanse();
            return v.RemoveData(dbc, vegobjekttypeid, kommune);
        }

        public OperationOutput UpdateVegobjectType(Db.DbCredentials dbc, int vegobjekttypeid, string vegobjekttype, int kommune)
        {
            OperationOutput output = new OperationOutput();
            bool keepUp = true;
            var url = CreateUrl(vegobjekttypeid, kommune);
            int returnert = 0;

            while (keepUp)
            {
                var webrequest = (HttpWebRequest) System.Net.WebRequest.Create(url);
                using (var response = webrequest.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = reader.ReadToEnd();

                        var rootObject = JsonConvert.DeserializeObject<RootObject>(result);
                        var metadata = JsonConvert.DeserializeObject<MetadataObject>(rootObject.metadata.ToString());

                        if ( metadata.returnert > 0 )
                        {
                            url = metadata.neste.href;
                            returnert += metadata.returnert;
                        }
                        else
                        {
                            output.SetData("returnert", returnert);
                            keepUp = false;
                            output.success = true;
                            return output;
                        }

                        //
                        // insert data
                        //
                        foreach (var item in rootObject.objekter)
                        {
                            if (item.egenskaper != null) {
                                var objektid = item.id;   // vegobjectid
                                var vegsegmenter = getVegsegmentObjekter(item.vegsegmenter);

                                var e = new Egenskaper();
                                var egenskaper = e.getEgenskaper(item.egenskaper);

                                output = InsertToTables(dbc, vegobjekttypeid, vegobjekttype, vegsegmenter, egenskaper, objektid, kommune);
                                if (!output.success)
                                    return output;
                            }
                        }
                    }
                }
            }
            output.SetData("returnert", returnert);

            return output;
        }

        private List<VegsegmentObject> getVegsegmentObjekter(List<object> d)
        {
            var result = new List<VegsegmentObject>();

            for (int i = 0; i < d.Count; i++)
            {
                Dictionary<string, object> collection = JsonConvert.DeserializeObject<Dictionary<string, object>>(d[i].ToString());
                var r = new VegsegmentObject();

                foreach (var item in collection)
                {
                    if (item.Key == "geometri")
                    {
                        var x = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(item.Value.ToString());
                        r.geometri = x.wkt.Value;
                    }
                    else if (item.Key == "kommune")
                    {
                        r.kommune = Convert.ToInt32(item.Value);
                    }
                    else if (item.Key == "fylke")
                    {
                        r.fylke = Convert.ToInt32(item.Value);
                    }
                    else if (item.Key == "vegreferanse")
                    {
                        Dictionary<string, object> collection2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString());

                        foreach (var item2 in collection2)
                        {
                            if (item2.Key == "kommune")
                                r.kommune = Convert.ToInt32(item2.Value);
                            if (item2.Key == "kategori")
                                r.kategori = item2.Value.ToString();
                            if (item2.Key == "status")
                                r.status = item2.Value.ToString();
                            if (item2.Key == "kommune")
                                r.kommune = Convert.ToInt32(item2.Value);
                            if (item2.Key == "fra_meter")
                                r.fra_meter = Convert.ToInt32(item2.Value);
                            if (item2.Key == "til_meter")
                                r.til_meter = Convert.ToInt32(item2.Value);
                            if (item2.Key == "meter")
                                r.meter = Convert.ToInt32(item2.Value);
                            if (item2.Key == "kortform")
                                r.kortform = item2.Value.ToString();
                        }
                    }
                }

                result.Add(r);
            }

            return result;
        }

        public OperationOutput CheckEgenskapTable(Db.DbCredentials dbc, int vegobjekttypeid)
        {
            OperationOutput output;

            var e = new Egenskaper();

            output = e.GetEgenskapTable(dbc, vegobjekttypeid);
            if (!output.success)
            {
                return output;
            }

            output = e.CheckEgenskapTable(dbc, vegobjekttypeid);
            if (!output.success)
            {
                return output;
            }

            return output;
        }

        internal OperationOutput InsertToTables(db.DbCredentials dbc, int objektypeid, string vegobjektype, List<VegsegmentObject> vegsegmenter, List<EgenskapObject> egenskaper, int objektid, int kommune)
        {
            var output = new OperationOutput();

            var v = new Vegreferanse();
            var e = new Egenskaper();

            foreach (var vegsegment in vegsegmenter)
            {
                var sql = v.SqlInsertVegsegment(vegsegment, objektid, objektypeid);
                output = Generic.DoSql(dbc, sql);
                if (!output.success)
                {
                    return output;
                }
                
                var sql2 = e.SqlInsertEgenskap(vegobjektype, vegsegment, objektid, kommune, egenskaper);
                output = Generic.DoSql(dbc, sql2);
                if (!output.success)
                {
                    return output;
                }
            }

            return output;
        }

        private string CreateUrl(int vegobjekttype, int kommune)
        {
            return String.Format("https://www.vegvesen.no/nvdb/api/v2/vegobjekter/{0}?inkluder=alle&srid=32633&antall=1000&kommune={1}", vegobjekttype, kommune);
        }
    }
}
