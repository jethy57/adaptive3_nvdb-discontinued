﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avinet.Adaptive.Modules.Nvdb
{
    #region Entities definition

    class VegObjekt
    {
        public int id { get; set; }
        public string href { get; set; }
        public object metadata { get; set; }
        public List<object> egenskaper { get; set; }
        public object segmentering { get; set; }
        public object geometri { get; set; }
        public object lokasjon { get; set; }
        public List<object> vegsegmenter { get; set; }
        public object relasjoner { get; set; }
    }

    class RootObject
    {
        public List<VegObjekt> objekter { get; set; }
        public object metadata { get; set; }
    }

    class RootVegobjekttyper
    {
        public int id { get; set; }
        public string navn { get; set; }
        public string beskrivelse { get; set; }
        public string stedfesting { get; set; }
        public string veiledning { get; set; }
        public string sosinavn { get; set; }
        public string sosinvdbnavn { get; set; }
        public int sorteringsnummer { get; set; }
        public List<object> egenskapstyper { get; set; }
        public object relasjonstyper { get; set; }
        public object styringsparametere { get; set; }
    }

    class VegsegmentObject
    {
        internal int objektid { get; set; }
        internal int objekttypeid { get; set; }
        internal int fylke { get; set; }
        internal int kommune { get; set; }
        internal string kategori { get; set; }
        internal string status { get; set; }
        internal int hp { get; set; }
        internal int fra_meter { get; set; }
        internal int til_meter { get; set; }
        internal int meter { get; set; }
        internal string kortform { get; set; }
        internal string geometri { get; set; }
    }

    class EgenskapObject
    {
        internal int id { get; set; }
        internal int datatype { get; set; }
        internal int desimaler { get; set; }
        internal string navn { get; set; }
        internal string sosinavn { get; set; }
        internal int feltlengde { get; set; }
        internal string verdi { get; set; }
    }

    class MetadataNesteObject
    {
        public string start { get; set; }
        public string href { get; set; }
    }

    class MetadataObject
    {
        public int returnert { get; set; }
        public MetadataNesteObject neste { get; set; }
    }

    class VegObjekter
    {
        public List<VegObjekt> objekter { get; set; }
    }

    #endregion
}
