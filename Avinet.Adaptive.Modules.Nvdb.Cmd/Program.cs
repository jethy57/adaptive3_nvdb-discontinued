﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avinet.Adaptive.Modules.Nvdb;
using Avinet.Adaptive.Utils;
using Cartomatic.Utils.OpsIO;
using Cartomatic.Utils;
using db = Cartomatic.Utils.Db;
using wu = Avinet.WebServices.Adaptive.Utils;
using log4net;
using log4net.Config;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Avinet.Adaptive.Modules.Nvdb.Cmd
{
    class Program 
    {
        static db.DbCredentials _context_dbc { get; set; }
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            log.Info("Start..");
            ConsoleUtils.WriteLine("Asplan Viak Internet - Commandline tool for creating NVDB tables... ", ConsoleColor.Yellow);

            if (args.Length == 0 ) {
                Console.WriteLine("Invalid number of arguments. Use /H for help"); 
            } else {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "/H" ) {
                        Console.WriteLine("Avinet.Adaptive.Modules.Nvdb.Cmd [/H] [/S] [/C]");
                        Console.WriteLine(" /H      Help");
                        Console.WriteLine(" /S      Silent mode - no info on screen");
                        Console.WriteLine(" /C      Command mode");
                        ConsoleUtils.WriteLine("Press key to finish..", ConsoleColor.Yellow);
                        Console.ReadKey();
                    }
                    else if (args[i] == "/S")
                    {
                        log.Info("Use silent mode..");
                        var mdbc = wu.GetMdbc();
                        Run(mdbc,true);
                    }
                    else if (args[i] == "/C")
                    {
                        log.Info("Use command mode..");
                        var mdbc = wu.GetMdbc();
                        Run(mdbc,false);
                        ConsoleUtils.WriteLine("Press key to finish..", ConsoleColor.Yellow);
                        Console.ReadKey();
                    }
                }
            }
            log.Info("End..");
        }

        static void Run(Db.DbCredentials dbc, bool silentmode)
        {
            OperationOutput output = new OperationOutput();

            string nvdb_kommuner = System.Configuration.ConfigurationManager.AppSettings["nvdb_kommuner"].ToString();
            string nvdb_vegobjekttyper = System.Configuration.ConfigurationManager.AppSettings["nvdb_vegobjekttyper"].ToString();
            string connectionstring = System.Configuration.ConfigurationManager.AppSettings["mdbc"].ToString();

            var msg = String.Format("Kommuner: {0}", nvdb_kommuner);
            if (!silentmode) ConsoleUtils.WriteLine(msg);
            log.Info(msg);

            msg = String.Format("Vegobjekttyper: {0}", nvdb_vegobjekttyper);
            if (!silentmode) ConsoleUtils.WriteLine(msg);
            log.Info(msg);

            msg = String.Format("ConnectionString: {0}", connectionstring);
            if (!silentmode) ConsoleUtils.WriteLine(msg);
            log.Info(msg);

            if (!silentmode) {
                ConsoleUtils.WriteLine("Press key to continue..", ConsoleColor.Yellow);
                Console.ReadKey();
            }

            var kommuner = nvdb_kommuner.Split(',').ToList();
            var vegobjekttypeider = nvdb_vegobjekttyper.Split(',').ToList();

            dbc.createConnectionString();

            if (!silentmode) ConsoleUtils.WriteLine("Check for existing database... ");

            var cls = new Avinet.Adaptive.Modules.Nvdb.Update();

            //
            // Clean up
            //
            output = cls.PrepareVegreferanse(dbc);
            if (!output.success)
            {
                log.Error(output.exception.msg);
                ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                return;
            }

            foreach (var item2 in vegobjekttypeider)
            {
                int vegobjekttypeid = Convert.ToInt32(item2);

                //
                // Check tables and remove index
                //
                output = cls.CheckEgenskapTable(dbc, vegobjekttypeid);
                if (!output.success)
                {
                    log.Error(output.exception.msg);
                    if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                    return ;
                }
                string egenskapstabell = output.GetData("vegobjekttype").ToString();

                output = cls.RemoveIndexForEgenskaper(dbc,egenskapstabell);
                if (!output.success)
                {
                    log.Error(output.exception.msg);
                    if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                    return;
                }

                //
                // Start update
                //
                foreach (var item in kommuner)
                {
                    var kommune = Convert.ToInt32(item);
                    msg = String.Format("Update vegobjekttypeid {0} named {1} in kommune {2}..", vegobjekttypeid, egenskapstabell, kommune);
                    log.Info(msg);
                    if (!silentmode) Console.Write(msg);

                    //
                    // Clean up
                    //
                    output = cls.PrepareKommuneAndEgenskap(dbc, vegobjekttypeid, egenskapstabell, kommune);
                    if (!output.success)
                    {
                        log.Error(output.exception.msg);
                        if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                        return;
                    }

                    //
                    // Add data
                    //
                    output = cls.UpdateVegobjectType(dbc, vegobjekttypeid, egenskapstabell, kommune);
                    if (!output.success)
                    {
                        log.Error(output.exception.msg);
                        if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                        return;
                    }
                    msg = String.Format(" (Added {0})", output.GetData("returnert").ToString());
                    log.Info(msg);
                    if (!silentmode) Console.WriteLine(msg);
                }

                //
                // Add index for egenskaper
                //
                output = cls.CreateIndexForEgenskaper(dbc, egenskapstabell);
                if (!output.success)
                {
                    log.Error(output.exception.msg);
                    if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                    return;
                }
            }

            //
            // Add index for vegreferanse
            //
            output = cls.CreateIndexForVegreferanse(dbc);
            if (!output.success)
            {
                log.Error(output.exception.msg);
                if (!silentmode) ConsoleUtils.WriteLine(output.exception.msg, ConsoleColor.Red);
                return;
            }

        }
    }
}
